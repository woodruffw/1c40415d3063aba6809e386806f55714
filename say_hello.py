from blight.action import ASAction

class SayHello(ASAction):
	def before_run(self, tool):
    	print(f"running {tool.wrapped_tool()} from {tool.cwd} on {tool.inputs}")
      
    def after_run(self, tool):
    	print(f"finished running {tool.wrapped_tool()} on {tool.inputs}")